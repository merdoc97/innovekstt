package com.tt.third;

import java.util.Arrays;

/**

 */
public class ArraySorting {
    public static void main(String... args) {
        Integer[] naturalInt = {0, 1, 2, 3, 4, 8, 5, 6, 7};
        sort(naturalInt);
        Arrays.stream(naturalInt).forEach(integer -> System.out.print(integer));
        System.out.println();
        Integer[] naturalIntSec = {0, 1, 2, 4, 4, 8, 5, 6, 7};
//        exception
        sort(naturalIntSec);
    }

    public static void sort(Integer[] arrays) {
        long distinct = Arrays.stream(arrays).distinct().count();
        if (arrays.length > distinct) throw new UnsupportedOperationException("not allowed sorting for duplicates");
        Arrays.sort(arrays, (o1, o2) -> o1 - o2);
    }
}
