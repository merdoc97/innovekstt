package com.tt.async.cache;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.junit.Assert;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

import static java.lang.System.currentTimeMillis;

/**
 * .
 */
public class AsyncCacheImpl<K, V> implements AsyncCache<K, V> {
    private final ConcurrentHashMap<K, Value<V>> cacheMap;

    private final long globalExpiration;

    public AsyncCacheImpl(long expiration) {
        this.globalExpiration = expiration;
        this.cacheMap = new ConcurrentHashMap<>();
    }

    @Override
    public CompletableFuture<V> getOrCompute(K key, Function<K, CompletableFuture<V>> supplier) throws ExecutionException {
        if (Objects.nonNull(key) && containsKey(key)) {
            return CompletableFuture.supplyAsync(() -> cacheMap.get(key).getValue());
        }
        if (Objects.isNull(supplier))
            return null;
        try {
            put(key, supplier.apply(key).get());
        }catch (Exception e){
            throw new ExecutionException(e.getCause());
        }
        return supplier.apply(key);

    }

    @Override
    public boolean containsKey(K key) {
        if (cacheMap.containsKey(key) && cacheMap.get(key).isExpired()) {
            cacheMap.remove(key);
            return false;
        }
        return cacheMap.containsKey(key);
    }

    private void put(K key, V value)  {
        cacheMap.put(key, new Value<>(value, globalExpiration));
    }


    @Getter
    @Setter
    @EqualsAndHashCode
    private static class Value<V> {
        private long expiration;
        private V value;

        public Value(V value, long expiration) {
            this.value = value;
            this.expiration = currentTimeMillis() + expiration;
        }

        public boolean isExpired() {
            return currentTimeMillis() > expiration;
        }
    }


    /**
     * simple test example
     * @param args
     * @throws Exception
     */
    public static void main(String... args) throws Exception {
        AsyncCacheImpl<String, String> cache = new AsyncCacheImpl<>(1000L);
        Assert.assertEquals("next",cache.getOrCompute("value", s -> CompletableFuture.supplyAsync(() -> "next")).get());
        Assert.assertEquals("next",cache.getOrCompute("value",null).get());
        Assert.assertTrue(cache.containsKey("value"));
        Thread.sleep(1000L);
        Assert.assertFalse(cache.containsKey("value"));

    }
}
