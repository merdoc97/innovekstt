package com.tt.async.cache;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

/**

 */
public interface AsyncCache<K,V> {

    CompletableFuture<V> getOrCompute(K key, Function<K, CompletableFuture<V>> supplier) throws Exception;
    boolean containsKey(K key);
}
