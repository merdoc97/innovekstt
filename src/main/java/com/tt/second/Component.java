package com.tt.second;

import java.util.List;
import java.util.Objects;

/**
 */
public class Component {
    private final List list;

    private Component(Builder builder) {
        this.list = builder.list;
    }

    public List getList() {
        return list;
    }

    public static class Builder {
//        все переменные в классе builder должны быть final
        private final List list;

//        метод withList лишний , у строителя е должен быть конструктор по умолчанию. В конкретном примере это лишнее так как один единственный параметр
        public Builder (List list) {
            if (Objects.isNull(list))
                throw new IllegalArgumentException("required field can't be null");
            this.list = list;
        }

        public Component build() {
            return new Component(this);
        }
    }

}

